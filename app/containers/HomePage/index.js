/*
 * login
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

import 'styles.css';

export class HomePage extends React.PureComponent { //eslint-disable-line react/prefer-stateless-function

	static contextTypes = {
		router: PropTypes.object
	};

	constructor(props) {
		super(props);

		this.state = {

		};
	}

	render() {

	    return (
	    	<div>
	    		<h5>KARMEQ</h5>
	    	</div>
	    );
	}
}

function mapStateToProps(state){
  return {
  };
}

export default connect(mapStateToProps, null) (HomePage);